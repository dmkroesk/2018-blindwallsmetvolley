package com.appsfromholland.blindwallsvolley;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//import com.squareup.picasso.Picasso;

import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by dkroeske on 19/02/2018.
 */

public class BlindWallGalleryAdapter extends ArrayAdapter<Mural> {

    public BlindWallGalleryAdapter(Context context, ArrayList<Mural> items) {
        super( context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        // Furby ophalen
        Mural mural = getItem(position);

        // View aanmaken of herbruiken
        if( convertView == null ) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.mural_listview_item,
                    parent,
                    false
            );
        }

        // Koppelen datasource aan UI
        TextView author = (TextView) convertView.findViewById(R.id.lvAuthorId);
        final ImageView thumbnail = (ImageView) convertView.findViewById(R.id.lvImageId);

        // Set Author
        author.setText(mural.getAuthor());

        // Set Image with picasso
        String imageUrl = mural.getImageUrl();
        Picasso.get().load(imageUrl).into(thumbnail);

        return convertView;
    }
}
