package com.appsfromholland.blindwallsvolley;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class BlindWallsApiManager {

    Context context;
    RequestQueue queue;
    BlindWallsApiListener listener;

    // Constructor
    public BlindWallsApiManager(Context context, BlindWallsApiListener listener) {
        this.context = context;
        this.queue = Volley.newRequestQueue(this.context);
        this.listener = listener;
    }

    // API call, haal alle muren op
    public void getWalls() {

        final String url = "https://api.blindwalls.gallery/apiv2/murals";

        JsonArrayRequest request = new JsonArrayRequest (

                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("VOLLEY_TAG", response.toString());

                        try {
                           // JSONArray jsonArray = new JSONArray(response);
                            for( int idx = 0; idx < response.length(); idx++) {
                                String author = response.getJSONObject(idx).getString("author");
                                String desc = response.getJSONObject(idx).getJSONObject("description").getString("nl");
                                JSONArray images = response.getJSONObject(idx).getJSONArray("images");

                                int index = new Random().nextInt(images.length());

                                String imageUrl = "https://api.blindwalls.gallery/" +
                                        images.getJSONObject(index).getString("url");

                                Mural mural = new Mural(author, desc, imageUrl);

                                listener.onMuralAvailable(mural);

                                Log.d("VOLLEY_TAG", mural.toString());
                            }
                        } catch (JSONException e) {

                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("VOLLEY_TAG", error.toString());
                        listener.onMuralError( new Error("Fout bij ophalen murals") );
                    }
                }
        );

        queue.add(request);
    }



    // API call, haal alle muren op
    public void getWalls1() {

        final String url = "https://api.blindwalls.gallery/apiv2/murals";

        JsonArrayRequest request = new JsonArrayRequest (

                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("VOLLEY_TAG", response.toString());
                        try {
                            for( int idx = 0; idx < response.length(); idx++) {
                                String author = response.getJSONObject(idx).getString("author");
                                String desc = response.getJSONObject(idx).getJSONObject("description").getString("nl");
                                JSONArray images = response.getJSONObject(idx).getJSONArray("images");

                                int index = new Random().nextInt(images.length());

                                String imageUrl = "https://api.blindwalls.gallery/" +
                                        images.getJSONObject(index).getString("url");

                                Mural mural = new Mural(author, desc, imageUrl);

                                Log.d("VOLLEY_TAG", mural.toString());
                            }
                        } catch (JSONException e) {

                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("VOLLEY_TAG", error.toString());
                    }
                }
        );

        queue.add(request);
    }
}
