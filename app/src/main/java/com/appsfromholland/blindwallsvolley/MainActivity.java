package com.appsfromholland.blindwallsvolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements BlindWallsApiListener {

    BlindWallsApiManager apiManager;

    ListView lv;
    ArrayList<Mural> murals;
    ArrayAdapter<Mural> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = findViewById(R.id.muralsListViewId);
        murals = new ArrayList<>();
//        adapter = new ArrayAdapter<Mural>(
//                this,
//                android.R.layout.simple_list_item_1,
//                murals);
        adapter = new BlindWallGalleryAdapter(this, murals);
        lv.setAdapter(adapter);


        apiManager = new BlindWallsApiManager(getApplicationContext(), this);
        apiManager.getWalls();

    }

    @Override
    public void onMuralAvailable(Mural mural) {
        murals.add(mural);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onMuralError(Error error) {
        Log.d("","");
    }
}
