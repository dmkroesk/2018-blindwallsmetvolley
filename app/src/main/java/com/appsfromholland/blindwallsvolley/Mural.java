package com.appsfromholland.blindwallsvolley;

import java.io.Serializable;

/**
 * Created by dkroeske on 19/02/2018.
 */

public class Mural implements Serializable {
    private String author;
    private String description;
    private String imageUrl;

    public Mural(String author, String description, String imageUrl) {
        this.author = author;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Mural{" +
                "author='" + author + '\'' +
                '}';
    }
}
