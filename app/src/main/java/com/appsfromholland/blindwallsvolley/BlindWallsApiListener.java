package com.appsfromholland.blindwallsvolley;

public interface BlindWallsApiListener {
    public void onMuralAvailable(Mural mural);
    public void onMuralError(Error error);
}
